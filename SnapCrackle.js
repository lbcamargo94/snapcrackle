//SnapCrackle Code

function snapCrackle(maxValue){
    let snap = "Snap";
    let crackle = "Crackle";
    let complete = "SnapCraclke";
    let sc_list = [];
    for (let counter = 1; counter <= maxValue; counter++){
        if (counter%5==0 && counter%2!=0){
            sc_list.push(complete);
            // console.log(sc_list);
        }else if(counter%5==0){
            sc_list.push(crackle);
            // console.log(sc_list);
        }else if(counter%2!=0){
            sc_list.push(snap)
            // console.log(sc_list)
        }else{
            sc_list.push(counter);
            // console.log(sc_list)
        }
    }
    // console.log(sc_list);
    return sc_list;
}

// Bonus: SnapCracklePrime Code
function snapCracklePrime(maxValuePrime){
    let prime = "Prime";
    let sc_listprime = [];
    for (let count =2; count < maxValuePrime; count++){
        sc_listprime.push (snapCrackle(count));
        if(maxValuePrime === 2){
            sc_listprime.push(prime);
        }else if(maxValuePrime > 1){
            if (maxValuePrime%count !==0){
                sc_listprime.push(prime);
            }
        }else{
            sc_listprime.push(count);
        }
        }
    return sc_listprime;
}